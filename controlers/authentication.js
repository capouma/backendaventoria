//REQUIRES
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const joi = require('joi')
const { get } = require('../repositories/userRepository.js')

//VALIDATES JOI
const schema = joi.object({ email: joi.string().email().required(), password: joi.string().min(5).required() })


async function login (req, res)
{
    try {
        const validation = await schema.validateAsync(req.body)
        const user = await get(validation)

        if (!user)
        {
            res.statusCode = 404
            res.send('No existe el usuario')
        }

        const isValidPassword = await bcrypt.compare(validation.password, user.password)

        if (!isValidPassword)
        {
            res.statusCode = 401
            res.send('El password no es válido')
        }

        const tokenPayload = { email: validation.email }

        const token = jwt.sign(
            tokenPayload,
            process.env.SECRET,
            { expiresIn: process.env.EXPIRE_TOKEN }
        )

        res.statusCode = 200
        res.send({ token })

    }
    catch (error)
    {
        res.statusCode = 500
        res.send('Error inesperado, revisa tu conexión a internet')
    }
}

module.exports = { login }