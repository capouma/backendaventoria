const { getConnection } = require('./connectors.js')

const insertIdReservationIdExperience = 'INSERT INTO reservas_experiencias (id_reserva, id_experiencia) VALUES (?, ?)'

async function insertReservationExperience (idReservation, idExperience)
{
    try
    {
        const connection = await getConnection()
        await connection.query(insertIdReservationIdExperience, [idReservation, idExperience])
    } catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo insertar los datos')
    }
}
module.exports = { insertReservationExperience }