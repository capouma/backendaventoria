const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');

const { getConnection } = require('./connectors.js')


const getDataUser = 'SELECT * FROM `usuarios` WHERE `email` = ?'
const insertUser = `INSERT INTO usuarios
                    (nombre, apellidos, email, password, telefono, direccion, dni, fecha_nacimiento, rol)
                    VALUES(?,?,?,?,?,?,?,?,?)`
const updateUser = 'UPDATE `usuarios` SET nombre = ?, apellidos = ?, password = ?, telefono = ?, direccion = ?, fecha_nacimiento = ?, rol = ?  WHERE id = ?'
async function get(dataUser)
{
    const connection = await getConnection()
    const [row] = await connection.query(getDataUser, dataUser.email)
    return row[0]
}

async function register(req, res)
{
    try
    {
        const connection = await getConnection()
        const passwordHash = await bcrypt.hash(req.body.password, 10);
        const dataUser = Object.values({...req.body, password: passwordHash})
    
        await connection.query(insertUser, dataUser)

        res.statusCode = 200
        res.send('Usuario creado')

    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo crear el usuario')
    }


}

async function isLogged(req, res, next) {

    try {
        const connection = await getConnection()
        const { authorization } = req.headers

        if (!authorization || !authorization.startsWith('Bearer '))
        {
            res.statusCode = 401
            res.send('Authorization header required')
        }

        const token = authorization.slice(7, authorization.length)
        const decodedToken = jwt.verify(token, process.env.SECRET)

        const [users] = await connection.query(getDataUser, decodedToken.email)

        if (!users || !users.length)
        {
            res.statusCode = 401
            res.send('El usuario no existe')
        }

        req.auth = decodedToken
        req.user = users
        next()
    } catch (err)
    {
        res.statusCode = 401
        res.send(err.message)
    }
}

async function editUser(req, res)
{
    try
    {
        const user = req.body
        delete user.email
        delete user.dni
        const connection = await getConnection()
        const passwordHash = await bcrypt.hash(user.password, 10);
        const dataUser = Object.values({...user, password: passwordHash, id: req.user[0].id})
    
        await connection.query(updateUser, dataUser)

        res.statusCode = 200
        res.send('Usuario actualizado')
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo actualizar los datos del usuario')
    }
}


module.exports ={ get, register,isLogged, editUser }