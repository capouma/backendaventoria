const dateFns = require('date-fns')
const { getConnection } = require('./connectors.js')


const insertReservation = 'INSERT INTO reservas (fecha_compra, fecha_actividad, precio_total, cantidad_reservada, estado) VALUES (?,?,?,?,?)'
const updateReservation = "UPDATE `reservas` SET estado = 'cancelada' WHERE id = ?"
const listReservation = 'SELECT * FROM `reservas` R INNER JOIN `usuarios_reservas` UR ON R.id = UR.id_reserva and UR.id_usuario = ?'

const { getExperiences } = require('./experiencesRepository.js')
const { insertReservationExperience } = require('./reservation_experiencesRepository.js')
const { insertUserReservation } = require('./user_reservationRepository.js')




async function reservation(req, res)
{
    try
    {
        const connection = await getConnection()

        const [dataExperience] = await getExperiences(req.body.nombre)

        const totalPrice = dataExperience[0].precio * parseInt(req.body.cantidad_reservada)

        const getIdReservation = await connection.query(insertReservation,
            [
                dateFns.format(new Date(), 'yyyy-MM-dd'),
                req.body.fecha_actividad,
                totalPrice,
                req.body.cantidad_reservada,
                'activa'
            ])

        await insertReservationExperience(getIdReservation[0].insertId,dataExperience[0].id)
        await insertUserReservation(parseInt(req.user[0].id), getIdReservation[0].insertId)

        res.statusCode = 200
        res.send('Reserva realizada con éxito')

    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo crear la reserva')
    }
}

async function cancelReservation(req, res)
{
    try
    {
        const connection = await getConnection()
        await connection.query(updateReservation, req.body.id)

        res.statusCode = 200
        res.send('Reserva cancelada')
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo cancelar la reserva, porfavor inténtelo más tarde')
    }
}

async function viewReservation(req, res)
{
    try
    {
        const connection = await getConnection()
    
        res.statusCode = 200
        const [row] = await connection.query(listReservation, req.user[0].id)
        res.send(row)
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send('No se pudo cancelar la reserva, porfavor inténtelo más tarde')
    }
}
module.exports = { reservation, cancelReservation, viewReservation }