
const { getConnection } = require('./connectors.js')

const insertIdUserIdReservation = 'INSERT INTO usuarios_reservas (id_usuario, id_reserva) VALUES (?,?)'

async function insertUserReservation(idUser, idReservation)
{
    try
    {
        const connection = await getConnection()
        await connection.query(insertIdUserIdReservation, [idUser, idReservation])
    }
    catch (error)
    {
        console.error(error.message);
        res.statusCode = 500
        res.send('No se pudo insertar los datos')
    }
}

module.exports = { insertUserReservation }