const bcrypt = require('bcryptjs')
const joi = require('joi')

const schemaUser = joi.object(
    {
        nombre: joi.string().required(),
        apellidos: joi.string().required(),
        email: joi.string().email().required(),
        password: joi.string().min(5).required(),
        telefono: joi.string().required(),
        direccion: joi.string().required(),
        dni: joi.string().required(),
        fecha_nacimiento: joi.date().required(),
        rol: joi.string()
    })
const schemaReservation = joi.object(
    {
        fecha_actividad: joi.date().required(),
        nombre: joi.string().required(),
        cantidad_reservada: joi.number().required()
    })
async function isValidUser(req, res, next)
{
    try
    {
        const validation = await schemaUser.validateAsync(req.body)
        next()
    }
    catch (error)
    {
        res.statusCode = 404
        res.send('Alguno de los datos introducidos no es válido')
    }
}
async function isValidReservation(req, res, next)
{
    try
    {
        const validation = await schemaReservation.validateAsync(req.body)
        next()
    }
    catch (error)
    {
        res.statusCode = 404
        res.send('Alguno de los datos introducidos no es válido')
    }
}

module.exports = { isValidUser, isValidReservation }