const { getConnection } = require('./connectors.js')

const getDataExperience = 'SELECT * FROM `experiencias` WHERE `nombre` = ?'

async function getExperiences(experience)
{
    try
    {
        const connection = await getConnection()
        return await connection.query(getDataExperience, experience)
    }
    catch (error)
    {
        res.statusCode = 401
        res.send('La experiencia no existe')
    }
}

module.exports = { getExperiences }