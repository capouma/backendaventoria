const express = require('express')

const { login } = require('./controlers/authentication.js')
const validations = require('./repositories/validations.js')
const user = require('./repositories/userRepository.js')
const reservations = require('./repositories/reservationRepository.js')


const app = express();
app.use(express.json())

app.post('/api/register', validations.isValidUser, user.register)
app.post('/api/login',login)
app.post('/api/reservation',user.isLogged,validations.isValidReservation, reservations.reservation)
app.post('/api/editUser', user.isLogged, validations.isValidUser, user.editUser)
app.patch('/api/cancelReservation',user.isLogged, reservations.cancelReservation)
app.get('/api/viewReservation', user.isLogged, reservations.viewReservation)



app.listen(3000, () => console.log('Escuchando!'))