require('dotenv').config()

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

async function notifyReservation(to, reserva) {

    const msg =
    {
        to: to,
        from: process.env.NOTIFY_SENDER,
        subject: 'Reserva confirmada',
        text: 'Reserva confirmada\n' + reserva,
        html: '<strong>Reserva confirmada<br>' + reserva + '</strong>'
      }
      try
      {
        await sgMail.send(msg)
      } catch (error)
      {
        console.error(error)
      }
}

module.exports = { notifyReservation }